// Project Name: Sokoban
// Author: Scott Reese
// Created: 12/4/18
// See readme.md for goals

//////////////////////////////////////
///////// GLOBAL VARIABLES //////////
////////////////////////////////////

const map = [
    "    WWWWW          ",
    "    W   W          ",
    "    WB  W          ",
    "  WWW  BWW         ",
    "  W  B B W         ",
    "WWW W WW W   WWWWWW",
    "W   W WW WWWWW  OOW",
    "W B  B          OOW",
    "WWWWW WWW WSWW  OOW",
    "    W     WWWWWWWWW",
    "    WWWWWWW        "
];                  

let winCounter = 0
let won = false

const destination = document.getElementById("main")
let player = document.createElement("div")
player.id = "player"

let playerRow = 0
let playerColumn = 0

//////////////////////////////////
////////// BUILD DOM ////////////
////////////////////////////////

buildDom()

// Iterates through map array and builds the DOM

function buildDom() {

    let maxRows = map.length
    let maxCols = map[0].length
    viewModel = Array(maxRows).fill(null).map(() => Array(maxCols).fill(null)) // intentionally global
    adjustDomWidth(maxCols)

    for (let rowLoc in map) {
        let row = map[rowLoc]

        for (let colLoc in row) {
            let cell = row[colLoc]
            let cellGraphic = document.createElement("div")

            viewModel[rowLoc][colLoc] = cellGraphic

            placeCellByType(cell, cellGraphic, rowLoc, colLoc)
        }
    }
}

// Adjusts the main & title div widths
// Inputs:: max columns of map

function adjustDomWidth(maxCols) {
    let width = maxCols * 50 + "px"
    const message = document.getElementById("message")
    message.style.width = width
    destination.style.width = width
}

// Determines class and places a div (cell)
// Inputs:: map value (cell) and new div (cellGraphic)

function placeCellByType(cell, cellGraphic, rowLoc, colLoc) {
    switch (cell) {
        case "W":   // Wall cell
            buildWallCell(cellGraphic)
            break

        case " ":   // Empty cell
        case "S":   // Player marker
            buildEmptyCell(cellGraphic)
            placeStartingPlayer(cell, cellGraphic, rowLoc, colLoc)
            break

        case "B":   // Cell starting with a crate
            buildEmptyCell(cellGraphic)
            placeCrate(cellGraphic)
            break

        case "O":   // Cell with a storage location
            buildEmptyCell(cellGraphic)
            placeStorageLocation(cellGraphic)
            break

        case "X":   // Starting cell with a storage location & crate
            buildEmptyCell(cellGraphic)
            placeStorageLocation(cellGraphic)
            placeCrate(cellGraphic)
            break
    }

}

// Builds the cells that represent the walls
// Inputs:: cell graphic (div)

function buildWallCell(cellGraphic) {
    cellGraphic.classList.add("wallCell")
    destination.appendChild(cellGraphic)

}

// Builds the cells that represents empty space
// Inputs:: cell graphic (div)

function buildEmptyCell(cellGraphic) {
    cellGraphic.classList.add("emptyCell")
    destination.appendChild(cellGraphic)
}

// When the starting location is found this places the player
// player inside that cell graphic
// Inputs:: map value (cell), graphic for cell, 
//          column & row locations

function placeStartingPlayer(cell, cellGraphic, rowLoc, colLoc) {
    if (cell === "S") {
        cellGraphic.appendChild(player)
        playerColumn = parseInt(colLoc)
        playerRow = parseInt(rowLoc)
    }
}

// Places inside the target cell a div that represents a crate
// Inputs:: cell graphic (div)

function placeCrate(cellGraphic) {
    let crate = document.createElement("div")
    crate.classList.add("crate")
    cellGraphic.appendChild(crate)
}

// Places inside the target cell a div that represents where to place the crates
// Inputs:: cell graphic (div)

function placeStorageLocation(cellGraphic) {
    let storageLocation = document.createElement("div")
    storageLocation.classList.add("storageLocation")
    cellGraphic.appendChild(storageLocation)
    winCounter++
}

////////////////////////////////
//////////// MAIN /////////////
//////////////////////////////

document.addEventListener('keydown', (event) => {

    if (won === false) {
        const keyName = event.key

        switch (keyName) {
            case "ArrowUp":
                move(-1, +0)
                break

            case "ArrowDown":
                move(+1, +0)
                break

            case "ArrowRight":
                move(+0, +1)
                break

            case "ArrowLeft":
                move(+0, -1)
                break
        }
        winCheck()
    }
})

////////////////////////////////////////////////////
///////////// SUPPORTING FUNCTIONS ////////////////
/////////////////////////////////////////////////

// Moves the crate and / or player if able.
// Inputs:: row or column change

function move(rowChange, columnChange) {
    // Player variables
    let playerTargetRow = playerRow + rowChange
    let playerTargetColumn = playerColumn + columnChange
    let playerTargetCell = viewModel[playerTargetRow][playerTargetColumn]
    // Crate variables
    let crate = checkForACrate(playerTargetCell)
    let crateTargetRow = playerRow + rowChange * 2
    let crateTargetColumn = playerColumn + columnChange * 2
    let behindCrate = null

    if (crate != null) {
        let boxTargetCell = viewModel[crateTargetRow][crateTargetColumn]
        behindCrate = checkForACrate(boxTargetCell)
    }

    let crateCanMove = confirmLegalCrateMove(crateTargetRow, crateTargetColumn, crate, behindCrate)
    let playerCanMove = checkForAWall(playerTargetRow, playerTargetColumn)

    let conditionsForCrateToMove = (playerCanMove == true && crateCanMove == true)
    let conditionsForPlayerToMove = (playerCanMove == true && crateCanMove == null) || (playerCanMove == true && crateCanMove == true)

    if (conditionsForCrateToMove) updateDom(crate, crateTargetRow, crateTargetColumn)
    if (conditionsForPlayerToMove) {
        playerRow = playerTargetRow
        playerColumn = playerTargetColumn
        updateDom(player, playerRow, playerColumn)
    }

}

// Checks if the cell contains a crate
// Inputs:: target cell
// Outputs:: target crate or null (nothing found)

function checkForACrate(cell) {
    let crate = null

    for (let i = 0; i < cell.childNodes.length; i++) {
        if (cell.childNodes[i].className == "crate") {
            crate = cell.childNodes[i]
            return crate
        }
    }
    return crate
}

// Conditions for a legal move of a crate: no wall and crate behind it
// Inputs:: target row & column, crate to be moved & crate behind it (if there is one)
// Outputs:: true if the crate can move or null if there is no crate

function confirmLegalCrateMove(crateTargetRow, crateTargetColumn, crate, behindCrate) {
    let crateCanMove = null
    if (crate != null) {
        crateCanMove = checkForAWall(crateTargetRow, crateTargetColumn) && behindCrate == null
        return crateCanMove
    } return crateCanMove
}


// Looks ahead and reports if the move the player
// wants to make is valid.
// Inputs:: targeted row & column
// Outputs:: true or false

function checkForAWall(row, column) {
    const target = viewModel[row][column]
    if (target.className == "emptyCell") { return true } else return false
}

// Moves the player or box
// Inputs:: object to be moved, target row & column

function updateDom(object, targetRow, targetColumn) {
    const target = viewModel[targetRow][targetColumn]
    target.appendChild(object)
}

// Check for the win condition & alerts the player
// if they won.

function winCheck() {
    let counter = 0

    viewModel.map(row => {
        row.map(box => {
            if (box.childElementCount == 2 && !box.contains(player)) counter++
        })
    })

    if (counter == winCounter) replacePlayer(playerRow, playerColumn)
}

// Replaces the player with the message "You Won!"
// Inputs:: target row & column

function replacePlayer(targetRow, targetColumn) {
    const textNode = document.createElement("div")
    const text = document.createTextNode("You Won!")
    textNode.appendChild(text)

    let targetedCell = viewModel[targetRow][targetColumn]
    targetedCell.removeChild(player)

    targetedCell.appendChild(textNode)
    won = true
}