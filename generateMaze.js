// row 0 && row 14 || col 0 && col 20
// bias startRow - finishRow = up or down
// bias towards finish diminishes after 3 attempts
// bias towards finish increases after 3 attempts
// once a direction is set a bias in that direction is set for 3 turns
// 4 attempts prior to dead end

// check two spaces from attempt to leave gaps

// left minus col direction
// right plus col direction
// up minus row direction
// down positive row direction

const maxRow = 15
const maxCol = 21

const startRow = 7
const startCol = 0

let rowFinish = 0
let colFinish = 0

let randomMap = new Array(maxRow);

let direction = "right"
let currentRow = startRow
let currentCol = startCol + 1

initializeMap()

function initializeMap() {

    const minFinish = 1
    const maxFinish = maxCol - 1

    const finishRow = 14
    let finishCol = randomFinish(minFinish, maxFinish)

    for (i = 0; i < randomMap.length; i++) { randomMap[i] = new Array(maxCol); }
    for (let row = 0; row < randomMap.length; row++) {
        for (let col = 0; col < randomMap[row].length; col++) {
            randomMap[row][col] = ""
            if (row == startRow && col == startCol) {
                randomMap[row][col] = "S"
            } else if (row == finishRow && col == finishCol) {
                randomMap[row][col] = "F"
                rowFinish = row
                colFinish = col
            } else if (row == startRow && col == startCol + 1) {
                randomMap[row][col] = " "
            }
            else {
                randomMap[row][col] = "W"
            }
        }
    }
    console.log(randomMap)
}

function randomFinish(min, max) {
    let random = Math.floor(Math.random() * (+max - +min)) + +min
    return random
}

function randomizeMap() {

    while (finishFound(currentRow, currentCol) == false) {
        determineRandomDirection()
        attemptMove()
    }

}

function finishFound(currentRow, currentCol) {
    if (currentRow == rowFinish && currentCol == colFinish) return true
}

function determineRandomDirection() {

    let attempts = 0
    const maxAttempts = 4

    let biasRight = .0125 * currentCol // 1.25% incremental bias to finish

    let bonusBias = .15
    let biasCounter = 1
    const biasMaxCounter = 3

    let crossRoads = []
}

let rand = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

////////
let generateWeighedList = function (list, weight) {
    let weighed_list = [];

    // Loop over weights
    for (let i = 0; i < weight.length; i++) {
        let multiples = weight[i] * 100;
        console.log(multiples)

        // Loop over the list of items
        for (let j = 0; j < multiples; j++) {
            weighed_list.push(list[i]);
        }
    }

    return weighed_list;
};
/////////////////

var list = ['javascript', 'php', 'ruby', 'python'];
var weight = [0.5, 0.2, 0.2, 0.1];
var weighed_list = generateWeighedList(list, weight);

var random_num = rand(0, weighed_list.length - 1);

console.log(weighed_list[random_num]);